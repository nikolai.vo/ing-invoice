import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { InvoicesPage } from '../invoices/invoices';
import { ProductInterface } from '../../model/product.interface';
import { InvoicrService } from '../../providers/invoice.service';


@Component({
  selector: 'page-create-product',
  templateUrl: 'create-product.html',
})
export class CreateProductPage {
  public products: Array<ProductInterface>;
  public invoices: any;
  public productArray: Array<ProductInterface>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public invoiceService: InvoicrService) {

    this.invoices = InvoicesPage;
    this.products = this.navParams.get('products');
    this.productArray = [];
    this.products.map(product => {
      product.quantity = 1;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateProductPage');
  }
  public incrementProduct(product: any) {
    product.quantity++;
  }

  public decrementProduct(product: any) {
    if (product.quantity > 1) {
      product.quantity--;
    }
  }
  public addProduct(product) {

    this.productArray.push(product);
    console.log(this.productArray)
    // if (product != null) {
    //   this.invoiceService.setProduct(this.productArray);
    // } else {
    //   this.invoiceService.removeProduct();
    // }
  }
  public addAllProducts() {
    // this.navCtrl.push(this.invoices)
    this.navCtrl.push(this.invoices, { product: this.productArray })
  }
}
