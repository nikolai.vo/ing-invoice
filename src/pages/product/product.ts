import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { tap } from 'rxjs/operators'

import { IdentityService } from '../../providers/identity.service';
import { ProductInterface } from '../../model/product.interface'



@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {
  public myForm: FormGroup;
  public apiRoot: string

  constructor(
    private http: HttpClient,
    private identityService: IdentityService
  ) {
    this.apiRoot = 'https://ing-invoicing.herokuapp.com/api';

    this.myForm = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        Validators.pattern('[a-zA-z]+')
      ]),
      price: new FormControl(null, [
        Validators.required,
        Validators.pattern(/^\d+$/)
      ]),
      currency: new FormControl(null, Validators.required)
    })
  }

  public onSubmit(values, valid: boolean) {
    if (valid == false) {
      console.log('error');
    }
    else {
      const getToken = this.identityService.getUserToken().then(token => {
        const headers = new HttpHeaders({
          'Authorization': token
        })

        return this.http.post<ProductInterface>(`${this.apiRoot}/products`, values,
          { headers: headers })
          .pipe(
            tap(
              res => {
                console.log(res);
              }
            )
          )
          .subscribe()
      })
    }
  }
}
