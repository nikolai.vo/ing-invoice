import { Component } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';

import { InvoicesPage } from '../invoices/invoices';
import { CustomerResponse } from '../../model/customer.interface';
import { InvoicrService } from '../../providers/invoice.service';
@Component({
  selector: 'page-create-invoice',
  templateUrl: 'create-invoice.html',
})
export class CreateInvoicePage {
  public invoices: any;
  public customers: CustomerResponse;
  public customerAdd: Array<CustomerResponse>;

  constructor(
    private navParams: NavParams,
    private navCtrl: NavController,
    public invoiceService: InvoicrService) {

    this.invoices = InvoicesPage;
    this.customers = this.navParams.get('customers');
    this.customerAdd = [];
    console.log(this.customers);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateInvoicePage');
  }
  addInvoice(customer) {
    this.customerAdd.push(customer);
    this.navCtrl.push(this.invoices, { customer: this.customerAdd })
    // if (customer != null) {
    //   this.invoiceService.setCustomer(this.customerAdd);
    // } else {
    //   this.invoiceService.removeCustomer();
    // }
  }
}
