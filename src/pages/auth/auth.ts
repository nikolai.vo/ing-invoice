import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';
import { IdentityService } from '../../providers/identity.service';

@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
})
export class AuthPage {

  public loginPage: any;
  public signUp: any;

  constructor(private navCtrl: NavController,
    private identity: IdentityService) {
    this.signUp = SignupPage;
    this.loginPage = LoginPage;
    this.identity.getUserToken();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AuthPage');
  }

  public onLogin() {
    this.navCtrl.push(this.loginPage);
  }

  public onSignup() {
    this.navCtrl.push(this.signUp);
  }

}
