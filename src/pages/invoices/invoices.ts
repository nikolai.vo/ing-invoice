import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { InvoicrService } from '../../providers/invoice.service';
import { IdentityService } from '../../providers/identity.service';
import { CreateInvoicePage } from '../create-invoice/create-invoice';
import { CustomerResponse } from '../../model/customer.interface';
import { ProductInterface } from '../../model/product.interface';
import { CreateProductPage } from '../create-product/create-product';

@Component({
  selector: 'page-invoices',
  templateUrl: 'invoices.html'
})
export class InvoicesPage {
  public createProductPage: any;
  public createInvoisePage: any;
  public apiRoot: string;
  public customer: Array<CustomerResponse> = [];
  public product: Array<ProductInterface> = [];

  constructor(
    private navCtrl: NavController,
    private identityService: IdentityService,
    private http: HttpClient,
    private navParams: NavParams,
    private invoiceService: InvoicrService) {

    this.apiRoot = 'https://ing-invoicing.herokuapp.com/api';
    this.customer = this.navParams.get('customer');
    this.product = this.navParams.get('product');
    this.createInvoisePage = CreateInvoicePage;
    this.createProductPage = CreateProductPage;


    // invoiceService.getCustomer().then(res => {
    //   this.customer = res;
    //   console.log(this.customer)
    // });

    // invoiceService.getProduct().then(res => {
    //   this.product = res;
    //   this.product.map(product => {
    //     product.quantity = 1;
    //   });
    // });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvoicesPage');
  }

  ngOnInit() {
    console.log(this.product)
  }
  public incrementProduct(product: any) {
    product.quantity++;
  }

  public decrementProduct(product: any) {
    if (product.quantity > 1) {
      product.quantity--;
    }
  }
  public onCreateInvoice() {
    return this.identityService.getUserToken().then(token => {
      const headers = new HttpHeaders({
        'Authorization': token
      })
      return this.http.get<CustomerResponse>(`${this.apiRoot}/customers`, { headers })
        .pipe(
          tap(
            (customers) => {
              this.navCtrl.push(this.createInvoisePage, {
                customers: customers
              });
            }
          )
        ).subscribe();
    })
  }

  public onCreateProduct() {
    return this.identityService.getUserToken().then(token => {
      const headers = new HttpHeaders({
        'Authorization': token
      })
      return this.http.get<ProductInterface>(`${this.apiRoot}/products`, { headers })
        .pipe(
          tap(
            (products) => {
              this.navCtrl.push(this.createProductPage, {
                products: products
              });
            }
          )
        ).subscribe()
    })
  }
}
