import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IdentityService } from '../../providers/identity.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CustomerInterface } from '../../model/customer.interface';
import { tap } from 'rxjs/operators';


@Component({
  selector: 'page-customer',
  templateUrl: 'customer.html',
})
export class CustomerPage {

  public myForm: FormGroup;
  public apiRoot: string;

  constructor(
    private identityService: IdentityService,
    private http: HttpClient) {

    this.apiRoot = 'https://ing-invoicing.herokuapp.com/api';

    this.myForm = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        Validators.pattern('[a-zA-z]+')
      ]),
      phone: new FormControl(null, [
        Validators.required,
        // Validators.pattern('\\+[0-9 ]+')
      ]),
      address: new FormControl(null, Validators.required)
    })
  }

  public onSubmit(values, valid: boolean) {
    if (valid == false) {
      console.log('error');
    }
    else {
      this.identityService.getUserToken().then(token => {
        const headers = new HttpHeaders({
          'Authorization': token
        });
        return this.http
          .post<CustomerInterface>(`${this.apiRoot}/customers`, values,
            { headers: headers })
          .pipe(
            tap(
              res => {
                console.log(res);
              }
            )
          )
          .subscribe()
      })
    }
  }
}
