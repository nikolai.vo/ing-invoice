export interface CustomerInterface {
    name: string,
    address: string,
    phone: string,

}
export interface CustomerResponse {
    name: string,
    address: string,
    phone: string,
    id: string,
}
