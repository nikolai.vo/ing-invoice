import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CustomerInterface } from '../model/customer.interface';

@Injectable()
export class CustomersService {
  public apiRoot: string;
  constructor(private http: HttpClient) {

    this.apiRoot = 'https://ing-invoicing.herokuapp.com/api';

  }

  public createCustomer$(customer: CustomerInterface) {
    const url = `${this.apiRoot}/customers`;
    return this.http.post<CustomerInterface>(url, customer);
  }
}
