import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { CustomerResponse } from '../model/customer.interface';
import { ProductInterface } from '../model/product.interface';
@Injectable()
export class InvoicrService {

  public customer: Array<CustomerResponse>;
  public product: Array<ProductInterface>;

  constructor(private storage: Storage) {
    this.customer = [];
    this.product = [];
  }

  public setCustomer(customer: Array<CustomerResponse>) {
    return this.storage.set('customer', JSON.stringify(customer));
  }
  public removeCustomer() {
    return this.storage.remove('customer');
  }
  public getCustomer() {
    return this.storage.get('customer').then(res => {
      res = JSON.parse(res);
      return res;
    });
  }

  public setProduct(product: Array<ProductInterface>) {
    return this.storage.set('product', JSON.stringify(product));
  }

  public removeProduct() {
    return this.storage.remove('product');
  }
  public getProduct() {
    return this.storage.get('product').then(res => {
      res = JSON.parse(res);
      return res;
    });
  }

}
