import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthPage } from '../pages/auth/auth';
import { CustomerPage } from '../pages/customer/customer';
import { ProductPage } from '../pages/product/product';
import { IdentityService } from '../providers/identity.service';
import { InvoicesPage } from '../pages/invoices/invoices';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  public rootPage: any;
  public customerPage: any;
  public productPage: any;
  public invoicesPage: any;


  @ViewChild('nav') nav: NavController;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private menuCtrl: MenuController,
    private identityService: IdentityService) {


    this.productPage = ProductPage;
    this.customerPage = CustomerPage;
    this.invoicesPage = InvoicesPage;

    this.identityService.getUserToken().then(token => {
      if(token && token != null && token != '') {
        this.rootPage = InvoicesPage;
      } else {
        this.rootPage = AuthPage;
      }
    })

    platform.ready().then(() => {

      statusBar.styleDefault();
      splashScreen.hide();
    });
  }


  public onLoad(page: any) {
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }

  public onLogout() {

    this.identityService.removeToken();
    this.identityService.removeUserId();
    this.nav.setRoot(AuthPage);
    this.menuCtrl.close();
  }

  // public onBackToInvoices() {
  //   this.nav.push(this.invoicesPage);
  //   this.menuCtrl.close();
  // }
}

